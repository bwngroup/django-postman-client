# Встановлення

Встановити використовуючи `pip`

```
pip install git+https://gitlab.com/bwngroup/django-postman-client.git
```
Додати `'postman_client'` до `INSTALLED_APPS`

```python
    INSTALLED_APPS = [
        ...
        'postman_client',
    ]
```
Промігрувати 
```
python manage.py migrate postman_client
```
Це створить таблицю `postman_client.MessageLog` де буде зберігатись результат відправки повідомлень. Модель підключена в адмінці.


# Налаштування

В налаштуваннях проекту вказати ключ авторизації отриманий на сайті Postman наступним чином:

```python
    POSTMAN_CLIENT = {
        'PROJECT_TOKEN': 'token'
    }
```

# Використання АПІ

Створити об'єкт клієнта:

```python
    from postman_client.api import Postman

    client = Postman()
```

1. [api/v1/email/](https://postman.org.ua/api/v1/email/) - відправити email лист:

```python
    client.send_email(data)
```
Вхідні дані це словник типу:

```json
    {
        "recipient": "test@example.com",
        "text": "Текст листа",
        "subject": "Заголовок листа"
    }
```
Якщо операція успішна повертає словник типу:

```json
    {
        "id": 47,
        "url": "http://postman.org.ua/api/v1/details/47/",
        "recipient": "test@example.com",
        "text": "Текст листа",
        "subject": "Заголовок листа",
        "created_at": "2019-08-19T19:36:08.351946Z",
        "sent_at": "2019-08-19T22:36:08.400437",
        "status_code": null,
        "status_comment": null,
        "repository_message": null
    }
```
Якщо сталась помилка повертає словник з описом помилки.
Результат запиту зберігається в таблиці `postman_client.MessageLog`.

2. [api/v1/sms/](https://postman.org.ua/api/v1/sms/) - відправити sms повідомлення:
```python
    client.send_sms(data)
```
Вхідні дані це словник типу:

```json
    {
        "recipient": "+380671234567",
        "text": "Текст повідомлення"
    }
```
Якщо операція успішна повертає словник типу:

```json
    {
        "id": 47,
        "url": "http://postman.org.ua/api/v1/details/47/",
        "recipient": "+380671234567",
        "text": "Текст повідомлення",
        "created_at": "2019-08-19T19:36:08.351946Z",
        "sent_at": "2019-08-19T22:36:08.400437",
        "status_code": null,
        "status_comment": null,
        "repository_message": null
    }
```
Якщо сервер повернув помилку то повертається словник з описом помилки.
Результат запиту зберігається в таблиці `postman_client.MessageLog`.


3. [api/v1/list/](https://postman.org.ua/api/v1/list/) - отримати список всіх повідомлень

```python
    client.messages_list()
```

4. [api/v1/list/{date}/](https://postman.org.ua/api/v1/list/2019-08-27/) - отримати список повідомлень за конктретний день

```python
    client.messages_list_for_date(date)
```
де на вхід передається об'єкт `datetime.date`.

5. [api/v1/details/{message_id}/](https://postman.org.ua/api/v1/details/1/) - отримати дані конкретного повідомлення

```python
    client.message_details(messsage_id)
```
де `message_id` ідентифікатор повідомлення. Його можна отримати в полі `id` в json-відповіді всіх попередніх запитів або в полі `postman_message_id` в моделі `MessageLog`. 
