#!/usr/bin/env python
from setuptools import setup, find_packages


setup(name='django-postman-client',
      version='0.0.1',
      description='Convenient API for Postman service.',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Topic :: Text Processing :: Linguistic',
      ],
      url='https://gitlab.com/bwngroup/django-postman-client',
      license='MIT',
      packages=['postman_client', ],
      install_requires=[
          'django>=1.11',
          'requests',
          'jsonfield==2.0.2'
      ],
      include_package_data=True,
      zip_safe=False)