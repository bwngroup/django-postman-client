from django.conf import settings

assert hasattr(settings, 'POSTMAN_CLIENT'), "Postman client is not configured." 


PROJECT_TOKEN = settings.POSTMAN_CLIENT.get('PROJECT_TOKEN', None)
POSTMAN_HOST = settings.POSTMAN_CLIENT.get('POSTMAN_HOST', 'https://postman.org.ua/')


assert PROJECT_TOKEN, "Postman client is not configured: PROJECT_TOKEN must be set."