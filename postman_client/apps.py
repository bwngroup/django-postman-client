from django.apps import AppConfig


class PostmanClientConfig(AppConfig):
    name = 'postman_client'
