import json 
import datetime

import requests

from .models import MessageLog
from . import settings as postman_conf


class Postman:
    def url(self, path):
        return postman_conf.POSTMAN_HOST + path 

    def get_headers(self):
        headers = {}
        headers['Authorization'] = f'Token {postman_conf.PROJECT_TOKEN}'
        headers['Content-Type'] = 'application/json'
        return headers

    def send_email(self, data):
        url = self.url('/api/v1/email/')
        headers = self.get_headers()
        request_body = json.dumps(data)

        response = requests.post(url, data=request_body, headers=headers)
        response_body = response.content
        response_dict = json.loads(response_body)
        
        self._write_to_log('email', data, response_dict) 

        return response_dict

    def send_sms(self, data):
        url = self.url('/api/v1/sms/')
        headers = self.get_headers()
        request_body = json.dumps(data)

        response = requests.post(url, data=request_body, headers=headers)
        response_body = response.content
        response_dict = json.loads(response_body)

        self._write_to_log('sms', data, response_dict) 

        return response_dict

    def messages_list(self):
        url = self.url('/api/v1/list/')
        headers = self.get_headers()
        response = requests.get(url, headers=headers)
        response_dict = json.loads(response.content)    
        return response_dict

    def messages_list_for_date(self, date:datetime.date):
        date_str = date.strftime('%Y-%m-%d')
        url = self.url('/api/v1/list/%s/' % date_str)
        headers = self.get_headers()
        response = requests.get(url, headers=headers)
        response_dict = json.loads(response.content)    
        return response_dict

    def message_details(self, message_id):
        url = self.url('/api/v1/details/%s/' % message_id)
        headers = self.get_headers()
        response = requests.get(url, headers=headers)
        response_dict = json.loads(response.content)    
        return response_dict

    def _write_to_log(self, msg_type, req_dict, resp_dict):
        MessageLog.objects.create(
            msg_type=msg_type,
            request_data=req_dict,
            response_data=resp_dict,
            created_at=resp_dict['created_at'],
            sent_at=resp_dict['sent_at'],
            status_code=resp_dict['status_code'],
            status_comment=resp_dict['status_comment'],
            postman_message_id=resp_dict['id']
        )
