from django.db import models
from jsonfield import JSONField


class MessageLog(models.Model):
    request_data = JSONField(
        verbose_name='Дані запиту'
    )  

    response_data = JSONField(
        verbose_name='Дані відповіді'
    )
    
    msg_type = models.CharField(
        max_length=10,
        verbose_name="Тип повідомлення"
    )

    postman_message_id = models.IntegerField(
        "Ідентифікатор повідомлення",
        help_text=("Цей ідентифікатор можна використати для отримання " 
                   "деталей цього повідомлення через АПІ Postman"),
    )

    created_at = models.DateTimeField(
        "Дата та час відправлення до серверу",
        auto_now_add=True,
        null=True,
    )
    
    sent_at = models.DateTimeField(
        "Дата та час опрацювання сервером",
        null=True, blank=True,
        default=None,
    )
    
    status_code = models.IntegerField(
        verbose_name="Код статусу",
        null=True,
    )
    
    status_comment = models.CharField(
        "Коментар до статусу",
        max_length=500,
        null=True
    )

    class Meta:
        db_table = 'postman_client_log'
        verbose_name = 'Історія запитів до Поштар'
        verbose_name_plural = 'Історія запитів до Поштар'
    